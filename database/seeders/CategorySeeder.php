<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Category;
use App\Models\Product;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $initCategories = [
            [
                "name" => "น้ำดื่ม/น้ำแร่",
                "products" => [
                    [
                        "name" => "น้ำดื่มเพียวไลฟ์ 1,500 มล",
                        "price" => 14
                    ],
                    [
                        "name" => "น้ำดื่ม SCG 1,500 มล",
                        "price" => 13
                    ],
                    [
                        "name" => "น้ำดื่มสิงฆ์ 1,500 มล",
                        "price" => 14
                    ],
                    [
                        "name" => "น้ำดื่มคริสตัล 1,500 มล",
                        "price" => 14
                    ],
                    [
                        "name" => "น้ำแร่ออรา 1,500 มล",
                        "price" => 23
                    ],
                    [
                        "name" => "น้ำแร่เพอร์ร่า 1,500 มล",
                        "price" => 19
                    ],
                    [
                        "name" => "น้ำดื่มพีเอชพลัส 550 มล",
                        "price" => 20
                    ]
                ]
            ],
            [
                "name" => "น้ำอัดลม/โซดา",
                "products" => [
                    [
                        "name" => "สไปรท์ 1.25 ลิตร",
                        "price" => 29
                    ],
                    [
                        "name" => "โค๊ก 1.25 ลิตร",
                        "price" => 29
                    ],
                    [
                        "name" => "แฟนต้าน้ำส้ม 1.25 ลิตร",
                        "price" => 29
                    ],
                    [
                        "name" => "แฟนต้าน้ำแดง 1.25 ลิตร",
                        "price" => 29
                    ],
                    [
                        "name" => "แฟนต้าน้ำเขียว 1.25 ลิตร",
                        "price" => 29
                    ]
                ]
            ],
            [
                "name" => "ชา/กาแฟ",
                "products" => [
                    [
                        "name" => "ฟูจิชะรสเทสตี้ 500 มล",
                        "price" => 30
                    ],
                    [
                        "name" => "ลิปตันเลมอน PET 445 มล",
                        "price" => 20
                    ],
                    [
                        "name" => "พอคคาชาเขียวมะลิ 500 มล",
                        "price" => 25
                    ],
                    [
                        "name" => "เนสกาแฟ ลาเต้ แคน",
                        "price" => 15
                    ],
                    [
                        "name" => "กาแฟเบอร์ดี้ เอสเพรสโซ่ 180 มล",
                        "price" => 15
                    ]
                ]
            ],
            [
                "name" => "สุขภาพ",
                "products" => [
                    [
                        "name" => "ซีวิต รสเลมอน 140 มล",
                        "price" => 16
                    ],
                    [
                        "name" => "ซีวิต รสส้ม 140 มล",
                        "price" => 16
                    ],
                    [
                        "name" => "วิตอะเดย์ซีพีช 480 มล",
                        "price" => 17
                    ],
                    [
                        "name" => "โคโค่แม็ก น้ำมะพร้าว 100%",
                        "price" => 25
                    ],
                    [
                        "name" => "คูคูริน น้ำมะนาวผสมน้ำผึ้ง 350 มล",
                        "price" => 22
                    ]
                ]
            ],
            [
                "name" => "ชูกำลังเกลือแร่",
                "products" => [
                    [
                        "name" => "เอ็ม-150 ออริจินัล 150 มล",
                        "price" => 10
                    ],
                    [
                        "name" => "สปอนเซอร์ขวด ออริจินัล 250 มล",
                        "price" => 10
                    ],
                    [
                        "name" => "คาราบาวแดง 150 มล",
                        "price" => 10
                    ],
                    [
                        "name" => "กระทิงแดง 150 มล",
                        "price" => 10
                    ],
                    [
                        "name" => "ลิโพวิตันดี 100 มล",
                        "price" => 12
                    ]
                ]
            ],
        ];

        foreach ($initCategories AS $iCategory => $category) {
            $id = Category::insertGetId(["name" => $category["name"]]);
            if (!empty($category["products"]) && count($category["products"]) > 0) {
                foreach ($category["products"] AS $product) {
                    Product::insert([
                        "category_id" => $id,
                        "name" => $product["name"],
                        "price" => $product["price"],
                    ]);
                }
            }
        }
    }
}
