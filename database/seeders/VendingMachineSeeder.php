<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\VendingMachine;

class VendingMachineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VendingMachine::factory()->count(10)->create();
    }
}
