<?php

namespace Database\Factories;

use App\Models\Lot;
use App\Models\VendingMachine;
use App\Models\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

class LotFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lot::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $vendingMachine = VendingMachine::inRandomOrder()->first();
        $product = Product::inRandomOrder()->first();

        return [
            'vending_machine_id' => $vendingMachine->id,
            'product_id' => $product->id,
            'quantity' => $this->faker->randomNumber(2, false),
        ];
    }
}
