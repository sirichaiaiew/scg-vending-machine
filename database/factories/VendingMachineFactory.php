<?php

namespace Database\Factories;

use App\Models\VendingMachine;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class VendingMachineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VendingMachine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $lat = 13.8058845;
        $long = 100.535343;

        return [
            "name" => $this->faker->city,
            "description" => $this->faker->address,
            "latitude" => $this->faker->latitude(
                $min = ($lat * 10000 - rand(0, 50)) / 10000,
                $max = ($lat * 10000 + rand(0, 50)) / 10000
            ),
            "longitude" => $this->faker->longitude(
                $min = ($long * 10000 - rand(0, 50)) / 10000,
                $max = ($long * 10000 + rand(0, 50)) / 10000
            ),
            "token" => null,
            "password" => Hash::make("Secret.Really!"),
            "token" => Str::random(60),
            "active" => 1
        ];
    }
}
