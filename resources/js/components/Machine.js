import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import _ from 'lodash';
import styled from 'styled-components';

function Machine() {
	// State
	const [categories, setCategories] = useState([]);

	// Effect
	useEffect(() => {
		getCategories();
	}, []);

	useEffect(() => {

	}, [categories]);

	// Function
	const getCategories = async () => {
		try {
			const response = await axios.get("/api/categories");
			if (response.status === 200 && response?.data?.length > 0) {
				setCategories(response.data);
			}
		} catch (error) {
			console.error(error);
		}
	}

	// Control
	const selectCategory = () => {

	}

	const selectProduct = () => {

	}

	console.log("categories", categories);

	return (
		<MachineContainer>
			<div className="container-fluid h-100">
				<div className="row h-100">
					<div className="col-xs-12 col-sm-6 col-md-8 h-100 p-0">
						<SelectorContainer>
							{
								(categories && categories.length > 0) &&
								<div className="row m-0">
									{
										categories.map((category) =>
											<div className="col-xs-6 col-sm-4 p-0">
												<Item>
													<div>
														<h4>{category.name}</h4>
													</div>
												</Item>
											</div>
										)
									}
								</div>
							}
						</SelectorContainer>
					</div>

					<div className="col-xs-12 col-sm-6 col-md-4 p-0">
						<CartContainer>

						</CartContainer>
					</div>
				</div>
			</div>
		</MachineContainer>
	);
}

const MachineContainer = styled.div`
	width: 100vw;
	height: 100vh;
	border: 5px solid red;
`;

const SelectorContainer = styled.div`
	background-color: green;
	width 100%;
	height: 100%;
	overflow-x: hidden;
	overflow-x: auto;
`;

const CartContainer = styled.div`
	background-color: yellow;
	width 100%;
	height: 100%;
`;

const CategoryContainer = styled.div`
	text-align: center;
	margin-top: 10px;
`;

const ProductContainer = styled.div`

`;

const Item = styled.div`
	display: table;
	width: 100%;
	padding: 10px;
	height: 150px;

	cursor: pointer;

	background-color: white;
	color: red;

	&:hover {
		background-color: red;
		color: white;
	}

	> div {
		text-align: center;
		display: table-cell;
		vertical-align: middle;
	}
`;

export default Machine;

if (document.getElementById('machine')) {
	ReactDOM.render(<Machine />, document.getElementById('machine'));
}
