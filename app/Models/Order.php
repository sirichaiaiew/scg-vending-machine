<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function vendingMachine()
    {
        return $this->hasOne(VendingMachine::class, 'id', 'vending_machine_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'order_id');
    }
}
