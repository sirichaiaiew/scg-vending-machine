<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public static function active()
    {
        return Category::where("active", 1)->get();
    }

    // Relation
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
