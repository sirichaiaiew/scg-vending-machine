<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendingMachine extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->belongsToMany(Product::class, 'lots', 'product_id', 'vending_machine_id');
    }
}
