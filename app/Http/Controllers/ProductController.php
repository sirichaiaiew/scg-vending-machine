<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\API;
use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    public function getCategories()
    {
        $status = null;
        $categories = Category::active();

        if (empty($categories) || count($categories) === 0) {
            $status = 404;
        }

        return response()->json($categories, $status ? $status : 200);
    }
}
